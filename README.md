# Gamification (CoSSMunity)

This is the initial implementation for a gamification server developed by a team of students from NTNU, it requires a modified version of EmonCMS in order to work.

The setup consist of four main components:

1. A LAMP stack
2. A database 
3. Some PHP-scripts
4. A modified version of EmonCMS

The server used in testing have been an Ubuntu 14.04 LTS server (64-bits), can be downloaded from http://www.ubuntu.com/download/alternative-downloads 

[Server setup](https://bitbucket.org/cossmic_release/gamificationserver/wiki/Server%20setup)

[Database documentation](https://bitbucket.org/cossmic_release/gamificationserver/wiki/Database%20documentation)

[Scripts](https://bitbucket.org/cossmic_release/gamificationserver/wiki/Scripts)

[EmonCMS module for CoSSMunity](https://bitbucket.org/cossmic_release/gamificationserver/wiki/Emoncms%20module%20for%20CoSSMunity)