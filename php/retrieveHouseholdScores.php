<?php
    //Fetches connection information from the config.ini file then sets the connection variables
    $iniArray = parse_ini_file("/var/www/html/config.ini", true);
    $hostname = $iniArray["connectionInfo"]["hostname"];
    $username = $iniArray["connectionInfo"]["username"];
    $password = $iniArray["connectionInfo"]["password"];
    $database = $iniArray["connectionInfo"]["database"];
    

    try{
        $dbh = new PDO('mysql:host='.$hostname.';dbname='.$database, $username, $password);
        if (isset($_GET["household_id"])) {
            $household_id = $_GET["household_id"];
            $sqlRetrieveHouseholdScores = "
                select value from household_scores where household_household_id = :household_id and score_type_score_type_id = '0'


            ";
            
            $retrieveHouseholdScores = $dbh->prepare($sqlRetrieveHouseholdScores);
            $retrieveHouseholdScores->bindParam(":household_id", $_GET["household_id"], PDO::PARAM_STR);
            $retrieveHouseholdScores->execute();
            $householdScores = $retrieveHouseholdScores->fetchAll(PDO::FETCH_ASSOC);
            $jsonHouseholdScores = json_encode($householdScores);
            if (isset($_GET["callback"])) {
                $callback = $_GET["callback"];
                echo $callback.'({"data":'.$jsonHouseholdScores.'});';
            }
            else{
                echo $jsonHouseholdScores;
            }}
        else{
            echo "Need the household_id of household making request to retrieve scores!";
        }
        $dhb = null;
        }
    catch(PDOExeption $e){
        echo '<h1>An error has occured.</h1><pre>', $e->getMessage(), '</pre>';
    }
 
?>
